//
//  SignupVC.swift
//  Task Manager
//
//  Created by a3331 on 1/20/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class SignupVC: UIViewController {

    
    @IBOutlet weak var txtemail: UITextField!
    
    @IBOutlet weak var txtcpassword: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var SignUpoutlet: UIButton!
    
    var uid:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.SignUpoutlet.layer.cornerRadius = 5.0
    }
    

    @IBAction func btnSignup(_ sender: Any) {
        if txtemail.text != nil && txtpassword.text != nil{
            Auth.auth().createUser(withEmail: txtemail.text!, password: txtpassword.text!){(result,error) in
                if error != nil{
                    print("erro")
                }else{
                    self.uid = (result?.user.uid)!
                    let ref = Database.database().reference(withPath: "users").child(self.uid)
                    ref.setValue(["email":self.txtemail.text!,"password":self.txtpassword.text!])
//                    ref.setValue(["password":self.txtpassword.text!])
                }
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginvc"{
            let taskvc = segue.destination as! AlltaskVC
            taskvc.userID = uid
            
        }
    }
}
