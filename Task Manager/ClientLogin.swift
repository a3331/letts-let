//
//  ClientLogin.swift
//  Task Manager
//
//  Created by a3331 on 3/13/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class ClientLogin: UIViewController {

    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBOutlet weak var btnlogOutlet: UIButton!
    
    
    var uid :String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.btnlogOutlet.layer.cornerRadius = 5.0
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // login button
    
    
    @IBAction func btnLogin(_ sender: Any) {
        
        if txtemail.text != nil && txtPassword.text != nil{
            
            Auth.auth().signIn(withEmail: txtemail.text!, password: txtPassword.text!){(result,error) in
                if error != nil{
                    print("erro")
                }else{
                    print("logged in")
                    self.uid = (result?.user.uid)!
                    
                    self.performSegue(withIdentifier: "loginClient", sender: self)
                    //                    let ref = Database.database().reference(withPath: "users").child(uid!)
                    //                    ref.setValue(["email":self.txtemail.text!])
                    //                    ref.setValue(["password":self.txtpassword.text!])
                }
                
            }
        }
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loginClient"{
            let taskvc = segue.destination as! TaskVC
            taskvc.userID = uid
            
        }
        
        //        let navigation = segue.destination as! UINavigationController
        //        let taskvc = navigation.topViewController as! AlltaskVC
        //        taskvc.userID = uid
        
        
    }
    
    // new
    
    func name()  {
        
    }
}
