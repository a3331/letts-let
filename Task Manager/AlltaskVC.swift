//
//  AlltaskVC.swift
//  Task Manager
//
//  Created by a3331 on 1/20/19.
//  Copyright © 2019 TechTeamAfrica. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth


struct Tasks {
    var isChecked :Bool
    var taskName: String
}
class AlltaskVC: UIViewController, UITableViewDataSource,UITableViewDelegate{
    
    
    @IBOutlet weak var viewTask: UIView!
    
    
    @IBOutlet weak var tblTask: UITableView!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    var taskArr:[Tasks]=[]
    
    var userID:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblTask.delegate =  self
        tblTask.dataSource = self
     //   viewTask.layer.cornerRadius = 3.0
       // tblTask.rowHeight = 80
        loadTasks()
        setWelcomeName()
        if let uid = userID{
            welcomeLabel.text = uid
        }
        // Do any additional setup after loading the view.
    }
    

    @IBAction func addTask(_ sender: Any) {
        
        let taskAlert = UIAlertController(title: "New Task", message: "Add a task", preferredStyle: .alert)
        
        taskAlert.addTextField()
        let addTaskAction = UIAlertAction(title: "Add", style: .default) { (action) in
            
           
            
            let taskText = taskAlert.textFields![0].text
            
            
           
            self.taskArr.append(Tasks(isChecked: false, taskName: taskText!))
             let ref = Database.database().reference(withPath: "users").child(self.userID!).child("Tasks")
            ref.child(taskText!).setValue(["isChecked":false])
            self.tblTask.reloadData()
        }
        
        let cancelAction =  UIAlertAction(title: "Cancel", style: .default)
        taskAlert.addAction(addTaskAction)
        taskAlert.addAction(cancelAction)
        
        present(taskAlert, animated: true, completion: nil)
    }
    

    @IBAction func logOutTask(_ sender: Any) {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    func setWelcomeName() {
        let userref = Database.database().reference(withPath: "users").child(userID!)
        userref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let email = value!["email"] as? String
            self.welcomeLabel.text = "Hello " + email! + " !"
        }
    }
    
    func loadTasks()  {
        let ref = Database.database().reference(withPath: "users").child(userID!).child("Tasks")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot]{
                let taskname = child.key
                
                let taskref = ref.child(taskname)
                taskref.observeSingleEvent(of: .value, with: { (tasksnapshot) in
                    let value = tasksnapshot.value as? NSDictionary
                    let isChecked = value!["isChecked"] as? Bool
                    self.taskArr.append(Tasks(isChecked: isChecked!, taskName: taskname))
                    self.tblTask.reloadData()
                })
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  taskArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "todocell", for: indexPath) as! AllTaskCell
        cell.tasklbl.text = taskArr[indexPath.row].taskName
    
        
        if taskArr[indexPath.row].isChecked{
            cell.imgChecked.image = UIImage(named: "checked.png")
        }else{
            cell.imgChecked.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let ref = Database.database().reference(withPath: "users").child(userID!).child("Tasks").child(taskArr[indexPath.row].taskName)
        if taskArr[indexPath.row].isChecked{
            taskArr[indexPath.row].isChecked = false
            
            ref.updateChildValues(["isChecked": false])
        }
        else{
            taskArr[indexPath.row].isChecked = true
            
            ref.updateChildValues(["isChecked": true])
        }
 
        tblTask.reloadData()
    
        
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            
            /**
             Simple Alert
             - Show alert with title and alert message and basic two actions
             */
            
            let taskkk = self.taskArr[indexPath.row].taskName
            let alert = UIAlertController(title: "Warning!", message: "You are about to delete \(taskkk), please confirm",         preferredStyle: UIAlertController.Style.alert)
                
            alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
                    //Cancel Action
                }))
                alert.addAction(UIAlertAction(title: "Yes",
                                              style: UIAlertAction.Style.default,
                                              handler: {(_: UIAlertAction!) in
                                                //Yes action
                                                
                                                let ref = Database.database().reference(withPath: "users").child(self.userID!).child("Tasks").child(self.taskArr[indexPath.row].taskName)
                                                
                                                ref.removeValue()
                                                self.taskArr.remove(at: indexPath.row)
                                                
                                                self.tblTask.reloadData()
                }))
                self.present(alert, animated: true, completion: nil)
           
            
            
            
           
        }
    }
}
